<?php
/**
 * Kindling Routing Functions.
 *
 * @package Kindling_Routing
 * @author  Matchbox Design Group <info@matchboxdesigngroup.com>
 */

use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;

/**
 * Sets up the router.
 */
function kindling_router_setup()
{
    // Create a service container
    $container = new Container;

    // Create a request from server variables, and bind it to the container; optional
    $request = Request::capture();
    $container->instance('Illuminate\Http\Request', $request);

    // Using Illuminate/Events/Dispatcher here (not required); any implementation of
    // Illuminate/Contracts/Event/Dispatcher is acceptable
    $events = new Dispatcher($container);

    // Create the router instance
    $router = new Router($events, $container);

    // Load the routes
    $routes_path = get_template_directory() . '/libs/routes.php';
    $routes_path = apply_filters('kindling_routing_routes_path', $routes_path);
    if (!file_exists($routes_path)) {
        return;
    }

    include_once $routes_path;

    // Dispatch the request through the router
    $response = $router->dispatch($request);

    // Send the response back to the browser
    $response->send();
}
