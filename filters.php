<?php
/**
 * Kindling Routing Filters.
 *
 * @package Kindling_Routing
 * @author  Matchbox Design Group <info@matchboxdesigngroup.com>
 */

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

if (!function_exists('add_action')) {
    return;
}

add_action('kindling_ready', function () {
    /**
     * Setup the router.
     */
    add_filter('do_parse_request', function ($do_parse, $wp) {
        // Let WordPress handle the "routing" in wp-admin
        if (is_admin()) {
            return true;
        }

        // If we are not in wp-admin then try first to find a matching route.
        // If no matching route is found then continue on with the normal
        // WordPress "routing".
        try {
            $wp->query_vars = [];
            kindling_router_setup();
            exit();
        } catch (NotFoundHttpException $e) {
            return true;
        }
    }, 10, 2);
});
